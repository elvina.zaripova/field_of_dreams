# - *- coding: utf- 8 - *-

import telebot

from telebot import types

import configparser

import random

config = configparser.ConfigParser()
config.read("config.py")

bot = telebot.TeleBot(config["DEFAULT"]["Token"])

questions_round = {
    "Холодное сладкое наслаждение в жаркое время года(еда)": "мороженое",
    "Разговор между двумя людьми": "диалог",
    "Неизменяемый список в Python": "кортеж",
    "Список 33 букв в русском языке": "алфавит",
    "Специалист по программированию и составлению программ": "программист",
    "Старинное название барометра": "буревестник",
    "Игра в Чехии, в ходе которой детей обучают правилам пожарной безопасности": "пламя",
    "Крупнейшая технологическая компания в Южной Корее": "самсунг",
    "Металл, открытый Гансом Кристианом Эрстедом в 1825 году": "алюминий",
    "Самая маленькая птица в мире": "колибри",
    "Мысль или деятельность, направленная к благополучию": "забота",
    "Чашеобразное приспособление, которое используется для мытья рук или небольших предметов": "раковина",
    "Выпуклая крыша в виде полушария": "купол",
    "Кто проделывает характерные дырки в сыре": "бактерии",
    "Какие мероприятия проводятся 2 разав год в Антарктиде": "марафон",
    "Наука, изучающая строение организма человек, его систем и органов": "анатомия",
    "Неприкосновенность частной жизни, которая обеспечивается правом человека на защиту личной жизни": "приватность",
    "Автомат, своими действиями производящий впечатление человеческой работы": "робот",
    "Жвачное парнокопытное млекопитающее с ветвистыми рогами": "олень",
    "Как у западных славян назывались селение, деревня, курень": "жупа",
    "Что использовали в Китае для глажки белья вместо утюга": "сковорода",
    "Любимец, пользующийся особым доверием": "наперсник",
    "Общедоступная многоязычная универсальная интернет-энциклопедия со свободным контентом": "википедия",
    "Одна из разновидностей грибковой инфекции, вызывается микроскопическими дрожжеподобными грибами": "кандидоз",
    "Древняя одежда, характерная для жителей Латинской Америки": "пончо",
    "Способность организма поддерживать свою целостность": "иммунитет"
}
dictionary_of_letters = {'0': 'а', '1': 'б', '2': 'в', '3': 'г', '4': 'д', '5': 'е', '6': 'ё', '7': 'ж', '8': 'з',
                         '9': 'и', '10': 'й', '11': 'к', '12': 'л', '13': 'м', '14': 'н', '15': 'о', '16': 'п',
                         '17': 'р', '18': 'с', '19': 'т', '20': 'у', '21': 'ф', '22': 'х', '23': 'ц', '24': 'ч',
                         '25': 'ш', '26': 'щ', '27': 'ъ', '28': 'ы', '29': 'ь', '30': 'э', '31': 'ю', '32': 'я'}

list_keys_dict = list(questions_round.keys())
keys_dict = random.choice(list_keys_dict)
question = keys_dict
answer = questions_round[question]


@bot.message_handler(commands=['start'])
def start(message):
    """
    The function allows you to send greetings to a bot message and start chatting with it.
    If you enter "/start" this function will automatically work.
    """
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.row("привет")
    bot.send_message(message.chat.id, 'Привет, это игра <Поле чудес>!\
    Правила.\
    Для начала игры вам нужно отправить боту команду "/start".\
    Далее ведущий вас поприветствует, и вам нужно нажать на появившуюся кнопку "привет". На\
    экране появится рандомный вопрос. Вам нужно будет в ходе игры угадать слово-ответ. У вас есть два выбора. Первый-\
    угадывать буквы постепенно, второй-угадать слово целиком. В случае первого выбора вам нужно будет вводить буквы до\
    тех пор, пока вам не придет в голову полноценное слово. Как вы только поняли какое это слово, вам нужно ввести его\
    целиком с маленькой буквы. Второй вариант: не перебирая буквы, ввести слово полностью. Если же бот в ответ\
    на введенное вами слово пришлет его форму, значит вы не угадали. Но в нашей игре нет существенных ограничений.\
    Поэтому вы можете как начать игру сначала, нажав на кнопку "/start", так и продолжить игру. За победу вы можете\
    получить выигрыш в виде хорошего настроения! Наш бот может отвечать на присланные пользователем картинки и стикеры.\
    Желаю вам удачной игры!', reply_markup=markup)


@bot.message_handler(commands=['help'])
def help(message):
    """
    The function, when the user enters the "/help" command, helps him by writing what to do when a problem occurs.
    """
    if message.text.lower() == "/help":
        bot.send_message(message.chat.id, "Ведите команду /start. Затем нажмите на кнопку 'привет'.")


@bot.message_handler(content_types=['text'])
def text(message):
    """
    This function is the main function of the program.
    The function checks if the user wants to enter the answer to the question as a whole word.
    If yes, he enters a word, if no, the user begins to enter letters.
    The function checks if each letter entered by the user matches a letter in the response word.
    If yes, then the user is informed that he guessed the letter, if no, the user can continue to guess.
    There is no limit to this.
    At the end, after the user responds in the form of a full word,
    the function gives him virtual gifts or memes that express 'sympathy'.
    """
    form = []
    for _ in answer:
        form.append('_')
    bot.send_message(message.chat.id, ' '.join(form))
    if message.text.lower() == "привет":
        bot.send_message(message.chat.id, question)
        a = telebot.types.ReplyKeyboardRemove()
        bot.send_message(message.chat.id, '😜', reply_markup=a)
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        markup_yes = types.KeyboardButton("да")
        markup_no = types.KeyboardButton("нет")
        markup.add(markup_yes, markup_no)
        bot.send_message(message.chat.id, "Хотите ли вы ввести целое слово?", reply_markup=markup)
    if message.text.lower() == "да":
        bot.send_message(message.chat.id, "Введите слово.Если бот вам не ответит, значит вы не угадали")
    if message.text.lower() == answer:
        bot.send_message(message.chat.id, "Ура!Вы угадали слово!Забирайте свой приз!")
        gift = 'Ваш приз!'
        lst = [open("images/car.jpg", "rb"), open("images/vacuum_cleaner.jpg", "rb"),
               open("images/hair_dryer.jpg", "rb"), open("images/mobile_phone.jpg", "rb"),
               open("images/TV.jpg", "rb"), open("images/headphones.jpg", "rb"),
               open("images/Washer.jpg", "rb"), open("images/car_2.jpg", "rb"), open("images/tablet.jpg", "rb")]
        picture = random.choice(lst)
        bot.send_photo(message.chat.id, picture, gift)
        b = telebot.types.ReplyKeyboardRemove()
        bot.send_message(message.chat.id, '🤩', reply_markup=b)
        c = telebot.types.ReplyKeyboardRemove()
        bot.send_message(message.chat.id, '🤩', reply_markup=c)
    elif message.text.lower() in dictionary_of_letters.values():
        if message.text.lower() in answer:
            bot.send_message(message.chat.id, "Ура!Вы угадали букву!🥳")
            for j in range(len(answer)):
                if message.text.lower() == answer[j]:
                    form.insert(j, answer[j])
                    bot.send_message(message.chat.id, 'Буква {} стоит на {} месте'.format((form[j]), j + 1))
        if message.text.lower() not in answer:
            bot.send_message(message.chat.id, "Увы...Вы не угадали букву. Нажмите '/start', если\
            вы хотите начать игру заново или можете продолжить угадывать буквы.")
            bot.send_message(message.chat.id, "Сегодня не Ваш день.")
            lst_losing = [open("images_after_losing/неудача.jpg", "rb"), open("images_after_losing/даладно.jpeg", "rb"),
                          open("images_after_losing/пронзительныйвзглядякубовича.jpg", "rb"),
                          open("images_after_losing/проигрыш.jpg", "rb"),open("images_after_losing/участие.jpg", "rb"),
                          open("images_after_losing/хех.jpg", "rb"), open("images_after_losing/рос.jpg", "rb")]
            picture_losing = random.choice(lst_losing)
            bot.send_photo(message.chat.id, picture_losing)


@bot.message_handler(content_types=['photo'])
def text_handler(message):
    """
    This function allows the bot to respond to pictures sent by the user.
    """
    lst_phrases = ['Великолепно!', 'Восхитительно!🥳', 'Мне такое не нравится(', 'Потрясающе',
                   'Мог бы выбрать картинку и получше', 'Люблю такое!)', '😣', '🙈', '❤']
    chat_id = message.chat.id
    bot.send_message(chat_id, random.choice(lst_phrases))


@bot.message_handler(content_types=["sticker"])
def give_stickers(message):
    """
    This function allows the bot to reply with stickers to the stickers sent by the user.
    """
    lst_stickers = [
        'CAACAgQAAxkBAAKBX1_XRRCV3sFd1j5PqFjnuPqkaT7rAAK8AANoVO4De15qj_SJlXEeBA',
        'CAACAgQAAxkBAAKBXF_XRProMpy378mjV6_aCAu7qb0JAAKnAANoVO4Dr_SGt-mgouceBA',
        'CAACAgQAAxkBAAKBWV_XROXnUAZsymKqvp2eB-N148YXAAKhAANoVO4D3Lvl-XNYfykeBA',
        'CAACAgIAAxkBAAKBVl_XRLhakmYdQ0-kb02jLPqj4FT2AAI8AAM7YCQUMbsRenGxPIQeBA',
        'CAACAgQAAxkBAAKAaF_WMJFZGQ0ffttCUWPDcJjAPm2OAAIuBAAC_jMNBseHpXMnlRrYHgQ',
        'CAACAgIAAxkBAAKAqV_WRI2pT0SzufH8JfBZ0VPOj6zRAAK1AAMlA1IP4mY7_iaCkmkeBA',
        'CAACAgIAAxkBAAKAr1_WTv7Ynf4FeCeRlN4rzNR1DtizAAIaBgACGELuCI3TjonlyFSBHgQ',
        'CAACAgIAAxkBAAKAvl_WUSAnZ4tkmt7WHaQJO_FkKNjzAAJuAwAC8n6CDIk5nJ7WTl-_HgQ',
        'CAACAgIAAxkBAAKAuF_WUQGDtngoBhw4WGY-ctIFKv4dAAISAgACNnYgDp7sZ7bv9NzVHgQ',
        'CAACAgIAAxkBAAKAtV_WUP_Ip_oe5mNFQefJaxd9ebxMAAIiAgACNnYgDjgrKIrs7Ue3HgQ',
        'CAACAgIAAxkBAAKAsl_WUNfrAAEbfMKihx5YBV2EQ4loCwACmAIAAlv_8grjFnGRgCZtRB4E',
        'CAACAgIAAxkBAAKAu1_WUQ21i2m6GbKX_LquaQwm61-uAALGAgAC7sShCiPsWTA_jFVwHgQ'
    ]
    stickers = random.choice(lst_stickers)
    bot.send_sticker(message.chat.id, stickers)


bot.polling(none_stop=True, interval=0)
