# Field_of_Dreams  

# We present the well-known game "Field of dreams".  
We implemented it in the form of bot telegrams. It can be played by people of different ages. It develops intelligence and vocabulary.
We came up with our own rules of the game. They are similar to the rules of the well-known game, but they have a number of differences: only one person plays, the bot acts as the lead, and the drum is not provided.  
# Game rules.  
To start the game, you need to send the bot the _"/start"_ command. Next, the host will greet you and you need to click on the "Hello" button that appears. A random question will appear on the screen. You will need to guess the answer word during the game.  
   
**You have two choices:**   
- The first is to guess the letters gradually.  
- The second is to guess the whole word.  
In the case of the first choice, you will need to enter letters until you come up with a full-fledged word.  
As soon as you understand what this word is, you need to enter it in its entirety with a small letter.  
The second option is to enter the entire word without going through the letters. If there is an error, start the game again by clicking on button _"/start"_.  
  
Our bot can also respond to images sent by the user.  
